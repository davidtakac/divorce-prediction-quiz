package com.example.divorcequiz.quiz.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.divorcequiz.R
import com.example.divorcequiz.base.refresh
import com.example.divorcequiz.common.interactor.QuizInteractor
import com.example.divorcequiz.common.model.*
import io.reactivex.Observable

class QuizViewModel (
    private val quizInteractor: QuizInteractor
): ViewModel() {
    val questionsLiveData = MutableLiveData<List<QuestionWrapper>>()
    val statusLiveData = MutableLiveData<Int>()

    fun onAnswerChecked(questionId: Int, answerId: Int, questionIdx: Int){
        questionsLiveData.value!![questionIdx].checkedAnswerId = answerId
        questionsLiveData.refresh()
    }

    fun getQuiz() = quizInteractor.getQuiz()
        .doOnNext {
            questionsLiveData.value = it.questions
                .mapIndexed {idx, q -> provideQuestionWrapper(q, idx) }
                .toList()
        }

    fun onSubmit(): Observable<QuizResults> {
        val allQuestionsAnswered = questionsLiveData.value!!.none { it.checkedAnswerId == -1 }

        if(!allQuestionsAnswered){
            statusLiveData.value = R.string.error_unanswered
            return Observable.empty()
        } else {
            return quizInteractor.submitQuiz(AnsweredQuiz(questionsLiveData.value!!
                .map { AnsweredQuestion(it.questionId, provideAnswerId(it)) }
                .toList())
            )
        }
    }

    private fun provideAnswerId(question: QuestionWrapper) =
        when(question.checkedAnswerId){
            R.id.rbZero -> question.answers[0].id
            R.id.rbOne -> question.answers[1].id
            R.id.rbTwo -> question.answers[2].id
            R.id.rbThree -> question.answers[3].id
            R.id.rbFour -> question.answers[4].id
            else -> -1
        }

    fun onReload() = getQuiz()

}