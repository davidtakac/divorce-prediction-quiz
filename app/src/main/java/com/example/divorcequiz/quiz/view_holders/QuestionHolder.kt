package com.example.divorcequiz.quiz.view_holders

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.example.divorcequiz.common.model.Answer
import com.example.divorcequiz.R
import com.example.divorcequiz.base.KotlinHolder
import java.lang.IllegalStateException

@EpoxyModelClass
abstract class QuestionModel: EpoxyModelWithHolder<QuestionHolder>(){
    @EpoxyAttribute var questionId: Int = -1
    @EpoxyAttribute lateinit var index: String
    @EpoxyAttribute lateinit var questionText: String
    @EpoxyAttribute lateinit var negativeLabel: String
    @EpoxyAttribute lateinit var positiveLabel: String
    @EpoxyAttribute lateinit var availableAnswers: List<Answer>
    @EpoxyAttribute var checkedAnswerId: Int = 1

    @EpoxyAttribute(hash = false) lateinit var answerCheckedListener: AnswerCheckedListener

    override fun getDefaultLayout(): Int = R.layout.cell_question

    override fun bind(holder: QuestionHolder) {
        holder.apply {
            tvQuestionNumber.text = index
            tvQuestion.text = questionText
            tvNegativeLabel.text = negativeLabel
            tvPositiveLabel.text = positiveLabel

            rgAnswers.apply {
                setOnCheckedChangeListener { group, checkedId ->
                    answerCheckedListener.onChecked(questionId, checkedId)
                }
                applyColors(this)
                /*removeAllViews()
                availableAnswers.forEach {
                    addView(provideRadioButton(context, it))
                }*/
                check(checkedAnswerId)
            }
        }
    }

    @SuppressLint("Range")
    private fun applyColors(rg: RadioGroup) {
        availableAnswers.forEachIndexed { index, answer ->
            val colorStateList = ColorStateList(
                arrayOf(intArrayOf(android.R.attr.checked), intArrayOf(-android.R.attr.checked)),
                intArrayOf(Color.parseColor(answer.color), Color.parseColor(answer.color))
            )
            val child = rg.getChildAt(index) ?: throw IllegalStateException("Answers size (${availableAnswers.size}) and number of radio buttons mismatch.")
            (child as RadioButton).buttonTintList = colorStateList
        }
    }

    @SuppressLint("Range")
    private fun provideRadioButton(context: Context, answer: Answer): RadioButton{
        val rb = RadioButton(context)
        rb.id = answer.id

        val colorStateList = ColorStateList(
            arrayOf(intArrayOf(android.R.attr.checked), intArrayOf(-android.R.attr.checked)),
            intArrayOf(Color.parseColor(answer.color), Color.parseColor(answer.color))
        )
        rb.buttonTintList = colorStateList

        return rb
    }
}

class QuestionHolder : KotlinHolder(){
    val tvQuestionNumber by bind<TextView>(R.id.tvQuestionNumber)
    val tvQuestion by bind<TextView>(R.id.tvQuestion)
    val tvNegativeLabel by bind<TextView>(R.id.tvNegativeLabel)
    val tvPositiveLabel by bind<TextView>(R.id.tvPositiveLabel)
    val rgAnswers by bind<RadioGroup>(R.id.rgAnswers)
}