package com.example.divorcequiz.quiz.activity

import android.content.Intent
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import com.airbnb.epoxy.TypedEpoxyController
import com.example.divorcequiz.quiz.view_holders.AnswerCheckedListener
import com.example.divorcequiz.R
import com.example.divorcequiz.base.BaseActivity
import com.example.divorcequiz.common.constants.RESULTS_KEY
import com.example.divorcequiz.common.model.QuestionWrapper
import com.example.divorcequiz.common.model.QuizResults
import com.example.divorcequiz.quiz.view_holders.answerHonestly
import com.example.divorcequiz.quiz.viewmodel.QuizViewModel
import com.example.divorcequiz.quiz.view_holders.question
import com.example.divorcequiz.results.activity.ResultsActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_quiz.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class QuizActivity : BaseActivity(), QuizInterface {

    override val layoutRes = R.layout.activity_quiz
    private val controller by inject<QuestionsController> { parametersOf(this) }
    private val viewModel by viewModel<QuizViewModel>()

    override fun onCreated() {
        super.onCreated()
        initViews()
    }

    private fun initViews(){
        rvQuestions.setController(controller)
        fabSubmit.setOnClickListener { viewModel.onSubmit()
            .doOnSubscribe { loadingVisible(View.VISIBLE) }
            .doOnEach { loadingVisible(View.GONE) }
            .doOnError { showSnackbar(R.string.error_submitting_quiz) }
            .subscribeByAndDispose(onNext = { startResultsActivity(it) })
        }

        viewModel.questionsLiveData.observe(this, Observer { controller.setData(it) })
        viewModel.statusLiveData.observe(this, Observer { showSnackbar(it) })

        viewModel.getQuiz()
            .doOnSubscribe { loadingVisible(View.VISIBLE) }
            .doOnEach { loadingVisible(View.GONE) }
            .doOnError { showSnackbar(R.string.error_receiving_quiz) }
            .subscribeByAndDispose()
    }

    private fun onReloadClicked() = viewModel.onReload()
        .doOnSubscribe { loadingVisible(View.VISIBLE) }
        .doOnEach { loadingVisible(View.GONE) }
        .doOnError { showSnackbar(R.string.error_reloading_quiz) }
        .subscribeByAndDispose()

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when(item.itemId){
            R.id.itemReload -> { onReloadClicked(); true }
            else -> super.onOptionsItemSelected(item)
        }

    override fun onAnswerChecked(questionId: Int, answerId: Int, questionIdx: Int) =
        viewModel.onAnswerChecked(questionId, answerId, questionIdx)

    private fun loadingVisible(visibility: Int) =
        progressBar.setVisibility(visibility)

    private fun showSnackbar(messageId: Int) =
        Snackbar.make(findViewById(R.id.quizCoordinator), messageId, Snackbar.LENGTH_SHORT).show()

    private fun startResultsActivity(results: QuizResults){
        val resultsString = Gson().toJson(results)
        val intent = Intent(this, ResultsActivity::class.java).putExtra(RESULTS_KEY, resultsString)
        startActivity(intent)
    }
}

interface QuizInterface{
    fun onAnswerChecked(questionId: Int, answerId: Int, questionIdx: Int)
}

class QuestionsController(private val qi: QuizInterface): TypedEpoxyController<List<QuestionWrapper>>(){
    override fun buildModels(questions: List<QuestionWrapper>) {
        answerHonestly { id("answer-honestly") }
        questions.forEachIndexed { idx, it ->
            question {
                id(it.questionId)
                questionId(it.questionId)
                index(it.index)
                questionText(it.questionText)
                negativeLabel(it.negativeLabel)
                positiveLabel(it.positiveLabel)
                availableAnswers(it.answers)
                checkedAnswerId(it.checkedAnswerId)
                answerCheckedListener(object: AnswerCheckedListener {
                    override fun onChecked(questionId: Int, answerId: Int) {
                        qi.onAnswerChecked(questionId, answerId, idx)
                    }
                })
            }
        }
    }
}