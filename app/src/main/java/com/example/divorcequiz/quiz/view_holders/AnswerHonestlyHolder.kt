package com.example.divorcequiz.quiz.view_holders

import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.EpoxyModelClass
import com.example.divorcequiz.R

@EpoxyModelClass
abstract class AnswerHonestlyModel: EpoxyModel<Any>(){
    override fun getDefaultLayout() = R.layout.cell_answer_honestly
}