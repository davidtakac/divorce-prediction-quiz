package com.example.divorcequiz.quiz.view_holders

interface AnswerCheckedListener {
    fun onChecked(questionId: Int, answerId: Int)
}