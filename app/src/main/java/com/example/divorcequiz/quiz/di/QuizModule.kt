package com.example.divorcequiz.quiz.di

import com.example.divorcequiz.common.interactor.QuizInteractor
import com.example.divorcequiz.quiz.activity.QuestionsController
import com.example.divorcequiz.quiz.activity.QuizInterface
import com.example.divorcequiz.quiz.viewmodel.QuizViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val quizModule = module {
    factory{(i: QuizInterface) ->
        QuestionsController(
            i
        )
    }
    factory{QuizInteractor(get())}
    viewModel { QuizViewModel(get()) }
}