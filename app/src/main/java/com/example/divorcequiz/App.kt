package com.example.divorcequiz

import android.app.Application
import com.example.divorcequiz.common.di.appModule
import com.example.divorcequiz.quiz.di.quizModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application(){
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(listOf(appModule, quizModule))
        }
    }
}