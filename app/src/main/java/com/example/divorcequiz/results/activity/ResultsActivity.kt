package com.example.divorcequiz.results.activity

import com.example.divorcequiz.R
import com.example.divorcequiz.base.BaseActivity
import com.example.divorcequiz.common.constants.RESULTS_KEY
import com.example.divorcequiz.common.model.QuizResults
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_results.*

class ResultsActivity : BaseActivity() {
    override val layoutRes = R.layout.activity_results

    override fun onCreated() {
        super.onCreated()
        initViews()
    }

    private fun initViews(){
        val results = Gson().fromJson(intent.getStringExtra(RESULTS_KEY), QuizResults::class.java)

        tvProbability.text = getString(R.string.probability, results.divorceProbability * 100)
        tvMessage.text = results.message
        btnRetakeQuiz.setOnClickListener { onBackPressed() }
    }
}