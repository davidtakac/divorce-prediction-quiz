package com.example.divorcequiz.common.model

import com.example.divorcequiz.R
import kotlinx.android.synthetic.main.cell_question.view.*

data class QuestionWrapper(
    val index: String,
    val questionId: Int,
    val questionText: String,
    val negativeLabel: String,
    val positiveLabel: String,
    val answers: List<Answer>,
    var checkedAnswerId: Int = -1
)

fun provideQuestionWrapper(question: Question, idx: Int) =
    QuestionWrapper(
        (idx+1).toString(),
        question.id,
        question.questionText,
        question.negativeText,
        question.positiveText,
        question.answers
        //R.id.rbTwo
    )