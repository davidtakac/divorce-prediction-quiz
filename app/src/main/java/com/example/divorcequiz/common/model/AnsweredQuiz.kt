package com.example.divorcequiz.common.model

import com.google.gson.annotations.SerializedName

data class AnsweredQuiz(
    @SerializedName("answers")
    val answeredQuestions: List<AnsweredQuestion>
)