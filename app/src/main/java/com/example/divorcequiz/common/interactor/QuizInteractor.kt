package com.example.divorcequiz.common.interactor

import com.example.divorcequiz.base.defaultConfig
import com.example.divorcequiz.common.model.AnsweredQuiz
import com.example.divorcequiz.common.rest_interface.QuizRestInterface

class QuizInteractor (private val restInterface: QuizRestInterface){

    fun getQuiz() = restInterface.getQuiz().defaultConfig()

    fun submitQuiz(quiz: AnsweredQuiz) = restInterface.submitQuiz(quiz).defaultConfig()

}