package com.example.divorcequiz.common.model

import com.example.divorcequiz.common.constants.EMPTY_STRING
import com.google.gson.annotations.SerializedName

data class QuizResults(
    @SerializedName("probability") var divorceProbability: Double = -1.0,
    @SerializedName("message") var message: String = EMPTY_STRING
    )