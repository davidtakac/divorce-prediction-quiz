package com.example.divorcequiz.common.constants

const val BASE_URL = "http://ruap-projekt.azurewebsites.net/"

const val NO_ID = -1
const val EMPTY_STRING = ""

const val RESULTS_KEY = "results_key"