package com.example.divorcequiz.common.model

import com.example.divorcequiz.common.constants.EMPTY_STRING
import com.example.divorcequiz.common.constants.NO_ID
import com.google.gson.annotations.SerializedName

data class Question(
    @SerializedName("questionID")
    var id: Int = NO_ID,
    @SerializedName("questionText")
    var questionText: String = EMPTY_STRING,
    @SerializedName("textPositive")
    var positiveText: String = EMPTY_STRING,
    @SerializedName("textNegative")
    var negativeText: String = EMPTY_STRING,
    @SerializedName("answers")
    var answers: List<Answer> = listOf()
)