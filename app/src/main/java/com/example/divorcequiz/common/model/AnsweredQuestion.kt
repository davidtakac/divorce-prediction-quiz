package com.example.divorcequiz.common.model

import com.google.gson.annotations.SerializedName

data class AnsweredQuestion(
    @SerializedName("questionID")
    val questionId: Int,
    @SerializedName("answerID")
    val answerId: Int
)