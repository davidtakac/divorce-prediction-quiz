package com.example.divorcequiz.common.model

import com.google.gson.annotations.SerializedName

data class Answer(
    @SerializedName("answerID")
    var id: Int = -1,
    @SerializedName("answerColor")
    var color: String = ""
)