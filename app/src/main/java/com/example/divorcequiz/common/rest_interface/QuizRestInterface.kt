package com.example.divorcequiz.common.rest_interface

import com.example.divorcequiz.common.model.AnsweredQuiz
import com.example.divorcequiz.common.model.QuizResponse
import com.example.divorcequiz.common.model.QuizResults
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface QuizRestInterface {

    @GET("api/quiz/get")
    fun getQuiz(): Observable<QuizResponse>

    @POST("api/quiz/submit")
    fun submitQuiz(@Body answeredQuiz: AnsweredQuiz): Observable<QuizResults>
}