package com.example.divorcequiz.common.model

import com.example.divorcequiz.common.constants.EMPTY_STRING
import com.google.gson.annotations.SerializedName

data class QuizResponse(
    @SerializedName("title")
    var quizTitle: String = EMPTY_STRING,
    @SerializedName("questions")
    var questions: List<Question> = listOf()
)