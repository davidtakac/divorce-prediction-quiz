package com.example.divorcequiz.base

import androidx.lifecycle.MutableLiveData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

fun <T> Observable<T>.defaultConfig() = subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

fun <T> MutableLiveData<T>.refresh(){
    value = value
}